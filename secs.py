#!/usr/bin/env python3

"""
secs v0.1 - Python script for secret management

Syntax:

    secs { add | get | set | del } TAG1 [TAG2 ...]

    secs { mkdatabase | passwd | ls [REGEX] }

Where:

  - add/get/set/del: manipulate secrets
  - mkdatabase: creates a new secret database in $SECS_DB
  - ls: outputs all tags or tags matching the REGEX
  - passwd: changes the passphrase

To disable incremental backups, pass the $SECS_NOBACKUPS environment variable.

Licensed under MIT (Expat)
Copyright (C) 2020 Karam Assany <karam.assany@tuta.io>
"""

import sys
import os
from getpass import getpass
from subprocess import run, CalledProcessError, DEVNULL
from tempfile import TemporaryDirectory
from time import time
from re import compile

def log(msg, e="\n"): print(msg, end=e, file=sys.stderr)

if len(sys.argv) < 2: # No commands given
    log(__doc__.strip())
    exit(0)

cmd = sys.argv[1]
args = sys.argv[2:]

if cmd not in ('add', 'get', 'set', 'del', 'mkdatabase', 'passwd', 'ls'):
    log("FATAL: Command '{}' not understood".format(cmd))
    log(__doc__.strip())
    exit(1)

if (cmd in ('add', 'get', 'set', 'del')) and not args: # No tags given
    log("FATAL: Too few arguments")
    log(__doc__.strip())
    exit(1)

if (cmd in ('mkdatabase', 'passwd')) and args:
    log("FATAL: Too many arguments")
    log(__doc__.strip())
    exit(1)

if (cmd == 'ls') and (len(args) > 1): # Multiple regexes given
    log("FATAL: Only one regex is allowed")
    exit(1)

if 'SECS_DB' in os.environ.keys():
    db_path = os.environ['SECS_DB']
else:
    log("FATAL: $SECS_DB (path to the secret database) is not set")
    log("Please specify it as an environment variable")
    exit(1)

if cmd == 'mkdatabase':
    if os.path.exists(db_path):
        log("FATAL: Something already exist in $SECS_DB: {}".format(db_path))
        exit(1)
elif not os.path.isfile(db_path):
    log("FATAL: The database does not exist at $SECS_DB: {}".format(db_path))
    exit(1)

# Passing SECS_NOBACKUPS disables incremental backups
inc_backups = 'SECS_NOBACKUPS' not in os.environ.keys()

if cmd == 'passwd':
    prompt = "Current passphrase: "
else:
    prompt = "Passphrase: "

try: # Obtaining the (new) passphrase
    if cmd == 'mkdatabase':
        is_correct = False
        while not is_correct:
            passwd = getpass("New passphrase: ", stream=sys.stderr)
            is_correct = getpass("Repeat new passphrase: ",
                                 stream=sys.stderr) == passwd
            if not is_correct:
                log("Passphrases are not identical. Try again")
    else:
        passwd = getpass(prompt, stream=sys.stderr)
except KeyboardInterrupt:
    exit(1)

is_changed = False # If true, the database will be updated/created

tmpdir_obj = TemporaryDirectory(suffix='A', prefix='Z')

tmpdir = tmpdir_obj.name
tmpname = os.path.basename(tmpdir)

# Loading the database

if cmd == 'mkdatabase':

    word = 'create'
    shadow = dict() # a blank database
    is_changed = True

else:

    word = 'update'

    try: # Decrypting
        run(['gpg', '-d', '--status-fd', '--with-colons', '--batch',
             '--passphrase', passwd, '-o', tmpdir + '/' + tmpname, db_path],
            stdin=DEVNULL, stdout=DEVNULL, check=True)
    except CalledProcessError:
        log("FATAL: Cannot decrypt the database. Try again")
        tmpdir_obj.cleanup()
        exit(1)

    try: # Decoding
        with open(tmpdir + '/' + tmpname) as f:
            shadow = eval( f.readlines()[0] )
    except:
        log("FATAL: The database is most likely corrupted")
        tmpdir_obj.cleanup()
        exit(1)

# Dealing with the database

if cmd == 'passwd':
    is_correct = False
    try: # Obtaining the new passphrase
        while not is_correct:
            passwd = getpass("New passphrase: ", stream=sys.stderr)
            is_correct = getpass("Repeat new passphrase: ",
                                 stream=sys.stderr) == passwd
            if not is_correct:
                log("Passphrases are not identical. Try again")
    except KeyboardInterrupt:
        tmpdir_obj.cleanup()
        exit(1)
    is_changed = True

elif cmd == 'ls':
    if args:
        log("Available fields matching '{}' [STDOUT]:".format(args[0]))
        print(' '.join(filter(compile(args[0]).search, shadow.keys())))
    else:
        log("Available fields [STDOUT]:")
        print(' '.join(shadow.keys()))

elif cmd == 'add':
    for arg in args:
        if arg in shadow.keys():
            log("Field '{}' already exists".format(arg))
        else:
            log("Value for new field '{}' [STDIN]: ".format(arg), e='')
            try:
                shadow[arg] = input()
            except KeyboardInterrupt:
                tmpdir_obj.cleanup()
                exit(1)
            is_changed = True

elif cmd == 'del':
    for arg in args:
        if arg not in shadow.keys():
            log("Field '{}' does not exist".format(arg))
        else:
            del shadow[arg]
            is_changed = True

elif cmd == 'set':
    for arg in args:
        if arg not in shadow.keys():
            log("Field '{}' does not exist".format(arg))
        else:
            log("Value for existing field '{}' [STDIN]: ".format(arg), e='')
            try:
                shadow[arg] = input()
            except KeyboardInterrupt:
                tmpdir_obj.cleanup()
                exit(1)
            is_changed = True

else: # cmd == 'get'
    for arg in args:
        if arg in shadow.keys():
            log("Value for existing field '{}' [STDOUT]:".format(arg))
            print(shadow[arg])
        else:
            log("Field '{}' does not exist".format(arg))

# Saving the database

if not is_changed:
    tmpdir_obj.cleanup()
    exit(0)

try:

    # Encoding
    with open(tmpdir + '/' + tmpname[::-1], 'w') as f:
        f.write(repr(shadow))

    # Backing up
    if not (cmd == 'mkdatabase'):
        if inc_backups:
            try:
                os.rename(db_path, '{}-{}.bak'.format(db_path, int(time())))
                log("A backup of the old database was created")
            except Exception as e:
                log("FATAL: Cannot backup the database. Changes are discarded.")
                log(format(e))
                tmpdir_obj.cleanup()
                exit(1)
        else:
            os.replace(db_path, db_path + '.bak')

    # Encrypting
    run(['gpg', '-c', '--status-fd', '--with-colons', '--batch',
         '--passphrase', passwd, '-o', db_path, tmpdir + '/' + tmpname[::-1]], 
        stdin=DEVNULL, stdout=DEVNULL, check=True)

    log("Database is {}d successfully".format(word))
    tmpdir_obj.cleanup()

except Exception as e:
    log("FATAL: Cannot {} the database".format(word))
    log(format(e))
    tmpdir_obj.cleanup()
    exit(1)
